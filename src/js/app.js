/*!
 *
 * ROSA v1.0.0
 *
 * MIT License
 * Author: Lanko Andrey (lanko@perevorot.com)
 *
 * В© 2015
 *
 */
(function (window, undefined) {
	"use strict";

	var APP = (function () {

		return {
			js: {
				contacts_map: function(_self){
					_self.find('.c-maps__map-wrap').each(function(){
						var self=$(this),
							center=[
								self.data('lat'),
								self.data('lng')
							];

						self.gmap3({
							center: center,
							zoom: self.data('zoom'),
							styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}]
						})
						.marker([{
							position: center,
							icon: '/app/img/design/contacts-map-icon.png'
						}]);
					});
				},
				nav_button: function(_self){
					$(_self).click(function() {
						$('body').toggleClass('is-active');
						$('.jsMobLangs').removeClass('is-active');
						setTimeout(function(){
							$('.c-h__show-sub').parent().removeClass('is-show');
						}, 100);
					})
				},
				homepage_grid: function(_self){
					var row=_self.find('.grid__row:first'),
							className=row.attr('class').split(' ')[1],
							image=$('body');

					image.addClass(className);
				},
				close_nav: function(_self){
					$(_self).click(function() {
						$('body').removeClass('is-active');
						$('.jsMobLangs').removeClass('is-active');
						setTimeout(function(){
							$('.c-h__show-sub').parent().removeClass('is-show');
						}, 100);
					})
				},
				show_sub: function(_self){
					$(_self).click(function() {
						$(_self).parent().toggleClass('is-show');
					})
				},
				show_langs: function(_self){
					$('.jsActiveLang').click(function() {
						$(_self).toggleClass('is-active');
					})
					$('.jsMobLang').click(function() {
						var jsSelectedMobLang = $(this).text();
						$('.jsActiveLang').text(jsSelectedMobLang);
						$(_self).removeClass('is-active');
					})
					$('.jsCloseMobLangs').click(function() {
						$(_self).removeClass('is-active');
					})
				},
				main_slider: function(_self){
					_self.slick({
						dots: true,
						infinite: true,
						autoplay: true,
						autoplaySpeed: 5000
					});
				},
				slider_product: function(_self){
					_self.slick({
						dots: true,
						infinite: true,
						autoplay: true,
						autoplaySpeed: 4000
					});
				},
				sb_gallery: function(_self){
					$(_self).lightGallery();
				},
				toggle_text_button: function(_self){
					$(_self).click(function() {
						$(_self).parent().prev().slideToggle(200);
						$(_self).toggleClass('is-active');
					})
				},
				emails: function(){
					$('.jsHideEmail').each(function(){
						var self=$(this);
						self.html(self.data('name')+'@'+self.data('domain'));
						self.parent().attr('href', 'mailto:' + self.data('name')+'@'+self.data('domain'));
					});
				},
				history: function(self) {
					$('a[href^="#"]').bind('click.smoothscroll',function (e) {
						e.preventDefault();
						$('.history__nav__year').removeClass('active');
						$(this).addClass('active');
						var target = this.hash,
							$target = $(target);

						$('html, body').stop().animate({
							'scrollTop': $target.offset().top-100
						}, 900, 'swing', function () {
							window.location.hash = target;
						});
					});

					var fh = $('.c-f').height();
				    $(".history__nav__wrap").sticky({
				    	topSpacing:50,
				    	bottomSpacing:fh + 100
				    });
				},
				js_products_groups: function(_self){
					var jsSubMenuList = _self;
					var jsSubMenuListMob = jsSubMenuList.clone();
					var jsSubMenuTarget = jsSubMenuList.prev().find('li.is-active');

					jsSubMenuList.appendTo( jsSubMenuTarget);

					jsSubMenuList.show();

				},
				js_cards_list: function(_self) {
					$(_self).each(function(){
						var jsCardsListItemCount = 0;
						var jsCardsListClass = 'c-cards__list--last-';
						var jsCardsListClassCounter,
							jsCardsListNumber = false;
						$(this).find('.c-cards__item').each(function() {
							jsCardsListItemCount++;
						});
						if($(this).hasClass('c-cards__list--3')) {
							jsCardsListNumber = 3;
						}
						jsCardsListItemCount = jsCardsListItemCount % jsCardsListNumber;
						if(jsCardsListItemCount != 0) {
							jsCardsListClass = jsCardsListClass + jsCardsListItemCount;
							$(this).addClass(jsCardsListClass);
						}

					});
				}

			},
		};
	}());

	$(function () {
		$("[data-js]").each(function () {
			var self = $(this);
			if (typeof APP.js[self.data("js")] === "function") {
				APP.js[self.data("js")](self, self.data());
			} else {
				console.log("No `" + self.data("js") + "` function in app.js");
			}
		});
	});

})(window);
